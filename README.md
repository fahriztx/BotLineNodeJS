Bot Line With NodeJS
 Created by **AlfathDirk** and **TCR TEAM**

# require node >= v8.x.x
check your nodejs version
`node -v`
[upgrade nodejs](https://google.com/search?q=upgrade+nodeJS)


How to run ?
------
- `git clone https://gitlab.com/fahriztx/BotLineNodeJS.git`
- `cd BotLineNodeJS && npm install`
- `npm start`

Every you run just command `npm start` on `BotLineNodeJS` Folder

Still work :construction_worker:
----
**TODO** features
- Implement All 
- Improve logic

Author
------
[@alfathdirk](https://instagram.com/alfathdirk)
[@fahriztx_](https://instagram.com/fahriztx_)
